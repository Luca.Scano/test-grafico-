// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GlassSpherePawn.generated.h"

UCLASS()
class TEST_CG_API AGlassSpherePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGlassSpherePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveSphereX(float x);
	void MoveSphereY(float y);
	void MoveSphere(float x, float y, float time);
	//Input functions

	void PitchCamera(float AxisValue);
	void YawCamera(float AxisValue);
	void ClickOn();
	void Zoom(float AxisValue);
	void Reset();
	void ScaleSphereUp();
	void ScaleSphereDown();
	void Orbit();
	void ClickOff();


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	//Dummy root component
	UPROPERTY(EditAnywhere)
		USceneComponent* planeRoot;
	UPROPERTY(EditAnywhere)
		USceneComponent* cameraRoot;
	UPROPERTY(EditAnywhere)
		USceneComponent* pitchController;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* PlaneMesh;
	UPROPERTY(EditAnywhere)
		class UCameraComponent* camera;
	UPROPERTY(EditAnywhere)
		float cameraDistance;
private:

	float xPos;
	float yPos;
	float xMousePos;
	float yMousePos;
	float radius;
	FVector2D  speed;
	FVector2D  position;
	float acceleration;
	float dragFactor;
	float sphereSize;
	float maxVelocity;
	float sphereSizeMultiplier;
	float ZoomFactor;
	FRotator yawRotation;
	FRotator pitchRotation;
	FVector2D MovementInput;
	FVector2D CameraInput;
	FVector2D CameraInputAtClick;
	FVector2D CameraAngles;
	FLinearColor sphereStruct;
	UMaterialInstanceDynamic* myMat;
	bool clicked;

};
