This is a project made in Unreal Engine, version 4.21.
It showcases a custom hlsl fragment shader that simulates a glass sphere on a plane.
A simple class allows the user to navigate around the object.

To simply try the demo navigate to the Build folder