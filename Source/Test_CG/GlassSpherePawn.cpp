// Fill out your copyright notice in the Description page of Project Settings.

#include "GlassSpherePawn.h"
#include "Classes/Components/InputComponent.h"
#include "Classes/Components/StaticMeshComponent.h"
#include "Classes/Materials/MaterialInstanceDynamic.h"
#include "Camera/CameraComponent.h"
#include "CameraCustomComponent.h"

// Sets default values
AGlassSpherePawn::AGlassSpherePawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;

#pragma region Setup of the plane
	radius = 5;
	acceleration = 3;
	maxVelocity = 20;
	dragFactor = 0.5f;
	sphereStruct = { xPos,yPos,radius,radius };
	planeRoot = CreateDefaultSubobject<USceneComponent>(TEXT("PlaneRoot"));
	RootComponent = planeRoot;
	PlaneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneMesh"));
	PlaneMesh->SetupAttachment(planeRoot);
	myMat = PlaneMesh->CreateDynamicMaterialInstance(0);
#pragma endregion

#pragma region Setup of the camera
	xPos = 0.0f;
	yPos = 0.0f;
	cameraDistance = 100;
	cameraRoot = CreateDefaultSubobject<USceneComponent>(TEXT("CameraRoot"));
	pitchController = CreateDefaultSubobject<USceneComponent>(TEXT("PitchController"));
	cameraRoot->SetupAttachment(planeRoot);
	pitchController->SetupAttachment(cameraRoot);
	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	FRotator NewRotation = FRotator(-90, 0, 0);
	FQuat QuatRotation = FQuat(NewRotation);
	FVector offset = FVector(0, 0, cameraDistance);
	camera->SetRelativeRotation(QuatRotation);
	camera->SetRelativeLocation(offset);
	camera->AttachToComponent(pitchController, FAttachmentTransformRules::KeepRelativeTransform);
#pragma endregion
}

// Called when the game starts or when spawned
void AGlassSpherePawn::BeginPlay()
{
	Super::BeginPlay();
}

#pragma region Sphere methods
void AGlassSpherePawn::MoveSphereX(float x)
{
	xPos = x;
}
void AGlassSpherePawn::MoveSphereY(float y)
{
	yPos = y;
}
void AGlassSpherePawn::MoveSphere(float x, float y, float time)
{
	FVector2D inputDirection = FVector2D(x, y);

	inputDirection *= time * acceleration;
	if ((speed + inputDirection * acceleration).Size() <= 0.1)
		speed = FVector2D(0, 0);

	else if (speed.Size() <= maxVelocity) {
		FVector2D dragVector = speed.GetSafeNormal()*(acceleration * dragFactor*time)*-1;
		speed += inputDirection + dragVector;
	}
	FVector2D appoPosition = position + speed;
	if (appoPosition.X<125 && appoPosition.X>-125 && appoPosition.Y <125 && appoPosition.Y>-125) {
		position += speed;

		sphereStruct = { position.X,position.Y,radius,radius };
		if (myMat != nullptr)
			myMat->SetVectorParameterValue("sphere", sphereStruct);
	}

	//UE_LOG(LogTemp, Warning, TEXT("%f %f"),position.X,position.Y);
}
void AGlassSpherePawn::Reset()
{

	xPos = 0;
	yPos = 0;
	position.X = xPos;
	position.Y = yPos;
	radius = 5;
	speed = FVector2D{ 0,0 };
	sphereStruct = { position.X,position.Y,radius,radius };

	myMat->SetVectorParameterValue("sphere", sphereStruct);


}
void AGlassSpherePawn::ScaleSphereUp()
{

	if (radius + 5 <= 50)
	{
		radius += 5;
		sphereStruct = { position.X,position.Y,radius,radius };

		myMat->SetVectorParameterValue("sphere", sphereStruct);
	}
}
void AGlassSpherePawn::ScaleSphereDown()
{
	if (radius - 5 >= 5)
	{
		radius -= 5;
		sphereStruct = { position.X,position.Y,radius,radius };

		myMat->SetVectorParameterValue("sphere", sphereStruct);
	}
}
#pragma endregion

#pragma region Camera Methods
void AGlassSpherePawn::PitchCamera(float AxisValue)
{
	CameraInput.Y = AxisValue;
}
void AGlassSpherePawn::YawCamera(float AxisValue)
{

	CameraInput.X = AxisValue;
}
void AGlassSpherePawn::ClickOn()
{
	clicked = true;
	CameraInputAtClick = CameraInput;
}
void AGlassSpherePawn::ClickOff()
{
	clicked = false;
}
void AGlassSpherePawn::Orbit()
{
	CameraAngles += CameraInput;
	yawRotation = FRotator(0, CameraAngles.X - CameraInputAtClick.X, 0);
	pitchRotation = FRotator(CameraAngles.Y - CameraInputAtClick.Y, 0, 0);

	cameraRoot->SetRelativeRotation(yawRotation);
	pitchController->SetRelativeRotation(pitchRotation);
}
void AGlassSpherePawn::Zoom(float AxisValue) {

	float appoDistance = cameraDistance + AxisValue;
	if (appoDistance < 120 && appoDistance>2 * radius)
	{
		cameraDistance += AxisValue;
		FVector offset = FVector(0, 0, -AxisValue);
		camera->AddRelativeLocation(offset * 2);
	}
}
#pragma endregion

// Called every frame
void AGlassSpherePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (myMat == nullptr)
		myMat = PlaneMesh->CreateDynamicMaterialInstance(0);

	MoveSphere(xPos, yPos, DeltaTime);

	if (clicked) {
		Orbit();
	}
}

// Called to bind functionality to input
void AGlassSpherePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGlassSpherePawn::MoveSphereX);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGlassSpherePawn::MoveSphereY);

	PlayerInputComponent->BindAxis("MouseAxisX", this, &AGlassSpherePawn::YawCamera);
	PlayerInputComponent->BindAxis("MouseAxisY", this, &AGlassSpherePawn::PitchCamera);

	PlayerInputComponent->BindAxis("ScrollAxis", this, &AGlassSpherePawn::Zoom);

	PlayerInputComponent->BindAxis("MouseAxisX", this, &AGlassSpherePawn::YawCamera);

	PlayerInputComponent->BindAction("Click", IE_Pressed, this, &AGlassSpherePawn::ClickOn);
	PlayerInputComponent->BindAction("Click", IE_Released, this, &AGlassSpherePawn::ClickOff);

	PlayerInputComponent->BindAction("SphereSizeUp", IE_Pressed, this, &AGlassSpherePawn::ScaleSphereUp);
	PlayerInputComponent->BindAction("SphereSizeDown", IE_Released, this, &AGlassSpherePawn::ScaleSphereDown);


	PlayerInputComponent->BindAction("Reset", IE_Pressed, this, &AGlassSpherePawn::Reset);

}

